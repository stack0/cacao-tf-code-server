module "app_proxy" {
    # source = "git::https://gitlab.com/cyverse/cacao-tf-os-ops.git//single-image-app-proxy?ref=2023-12-13"
    source = "git::https://gitlab.com/cyverse/cacao-tf-os-ops.git//single-image-app-proxy?ref=js2"

    # this is an example of hardcoding the distro because you don't want people to change it
    image_name = "Featured-Ubuntu22"
    proxy_auth_type = "basicauth"
    proxy_target_port = 8080
    # proxy_auth_user = "appuser"

    # add this to your inputs.tf
    username = var.username
    region = var.region
    project = var.project
    instance_name = var.instance_name
    instance_count = 1
    flavor = var.flavor
    keypair = var.keypair
    power_state = var.power_state
    user_data = var.user_data
    proxy_auth_user = local.proxy_auth_user
    proxy_auth_pass = var.proxy_auth_pass # leaving this blank will generate a random password
    proxy_expose_logfiles = var.proxy_expose_logfiles
}

resource "null_resource" "codeserver" {
  count = var.power_state == "active" ? 1 : 0

  triggers = {
    always_run = "${timestamp()}"
  }

  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ANSIBLE_SSH_PIPELINING=True ANSIBLE_CONFIG=ansible.cfg ansible-playbook -i ${module.app_proxy.ansible_inventory_path} --forks=10 playbook.yaml"
    working_dir = "${path.module}/ansible"
  }

  depends_on = [
    local.ansible_execution_completed
  ]
}

resource "local_file" "codeserver_config" {
    content = templatefile("${path.module}/config.yaml.tmpl",
    {
      cacao_user = var.username
    })
    filename = "${path.module}/ansible/config.yaml"
}

locals {
  # this is js2 only? need to confirm with regional clouds
  proxy_auth_user = var.proxy_auth_user != "" ? var.proxy_auth_user : var.username
  ansible_execution_completed = module.app_proxy.ansible_execution_completed
}


output "ansible_inventory_path" {
  description = "path to the ansible inventory file"
  value = module.app_proxy.ansible_inventory_path
}
output "instance_ips" {
  description = "IP addresses for all instances"
  value = module.app_proxy.instance_ips
}

output "instance_public_endpoints" {
  description = "Public endpoints for all instances"
  value = module.app_proxy.instance_public_endpoints
}

